#this is a tutorial in scraping web data in python
#this program scrapes NBA player data from the NBA draft records, from basketball-reference.com
#the tutorial followed can be found here:
#http://nyloncalculus.com/2015/09/07/nylon-calculus-101-data-scraping-with-python/

#NOTE: I've heard rumours about bball-ref IP banning for doing a bunch of scraping off their website.
#Should probably find a way to scrape of the NBA stats website, or look up some more on those rumours.

from urllib.request import urlopen
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np

#enter the url you're scraping
url = "http://www.basketball-reference.com/draft/NBA_2014.html"

#get the html code from the url stated above
html = urlopen(url)

#Create a BeautifulSoup object (soup) by passing through the html variable to BS's constructor function
soup = BeautifulSoup(html, "html.parser")

type(soup)

#The output of this file, we want it to be in a table. To get the column headers, look at the table header HTML tags
#(i.e. the content between <th></th> tags
#This should get loaded into a DataFrame object.
#If you're extracting player info,
#you need to inspect the html web page to find out where it is, so you know how to scrape it.
#In this instance, if you need the element "Player" from a table,
#you'd inspect the table element and find that the word "Player" is in the second table row (<tr></tr>).

#so, the element we want is within the first 2 rows of a page, so we look for 2 ROWS in the soup object.
#soup.findAll('tr', limit=2)
#1st parameter is keywords you want, 2nd is the number you want to find.
#findAll returns an array of Tag objects, according to the input parameters.
#As findAll returns an array, and the info we want is in the second row,
#we use the second element of the reutrned array.
#findAll('tr', limit=2)[1]
#This returns a single Tag object, so you can then use findAll within this tag object to get the table header tag.
#findAll('tr', limit=2)[1].findAll('th')
#this returns a tag object of whats within the first header tag of the second row tag.
#to get the text from each of these, we can use getText upon this tag object to get the column headers in the row.
column_headers = [th.getText() for th in
                  soup.findAll('tr', limit=2)[1].findAll('th')]

#now we have the headers, we need to fill the DataFrame object with the data from the page.
#The data is constructed with one draftee per row.
#But, as we extracted the first two rows previously, we should skip these rows.
#[2:] will start adding the rows, starting from the third occurance.
data_rows = soup.findAll('tr')[2:]
type(data_rows)

#The player data, unlike the column headers, is a 2-dimensional format.
#This loop is:
#For every row in data_rows (starting from the 3rd row, as stated above), find every cell (<td>) in the row.
#Do this for every row scraped within data_rows
player_data = [[td.getText() for td in data_rows[i].findAll('td')]
               for i in range(len(data_rows))]

#Now, we must add this player_data and column_headers as the parameters to a DataFrame to keep all this data
df = pd.DataFrame(player_data, columns=column_headers)

#Now, we need to find rows that contain nothing but nothing but null values. Pandas has an isnull() method to do this.
# isnull() = True when there is a null, false when there is not. The inverse is true for notnull()
#We will take out rows which have a null 'Player' entry.
#the df list will equal the previous instance of df, but without any entries with a null 'Player' field
df = df[df.Player.notnull()]

#After removing the nulls, we need to rename all the columns (such as 'Pk' to 'Player') in the table.
#This renames the "WS/48" column to "WS_per_48" for Winshares per 48.
#The rename function takes a term to search for in df, then the second parameter is the replacement text.
df.rename(columns={'WS/48':'WS_per_48'}, inplace=True)

#We can also use replace() to replace '%' with '_Perc' in column titles
df.columns = df.columns.str.replace('%', '_Perc')

#we also change the "per game" column titles to indicate they're per game states.
#The per game stats are in columns 14-18 (if starting from 0), so we select that range to append "_per_G" onto.
df.columns.values[14:18] = [df.columns.values[14:18][col] + "_per_G" for col in range(4)]

#After this, we need to change the data types of the elements of the DataFrame (the columns) to the proper kind of object
#the convert_objects() function can convert columns with numeric data to numeric types with convert_numeric=True parameter.
df = df.apply(pd.to_numeric, errors='coerce')

#Now, there are still some columns (Yrs, MP) that should be numbers but were passed over by convert_objects
#This was because they contained some non-numeric data, such as when a player didn't end up playing in the NBA.
#We can replace these blanks with zeroes, using fillna() to fill them with zeroes
df = df[:].fillna(0)

#Now we need to convert these leftover objects to integers.
#loc[a:b, x:y] takes a = first_row, b = last_row, x = first_column, y = last_column to replace objects in.
#as we're just taking the very first row (column headers), a and b can be left blank.
#The columns we want are between 'Yrs' and 'AST'
#df.loc[:,'Yrs':'AST'] = df.loc[:'Yrs':'AST'].astype(float64)

#Now, we should add a Draft Year column as we're getting draft into from more than one year's draft.
#insert(location, name, value) will add this column to the start of the table.
df.insert(0,'Draft_Yr', 2014)

#We can also delete the 'Rk' column, as its redundant with the'Pk' column existing.
#drop(name, axis_to_drop, inplace) can do this
df.drop('Rk', axis='columns', inplace=True)

#So far, all this has been done for a single draft class. Now, we need to design a template to repeatedly do this for multiple drafts.
#This uses the same approach as done previously.
#{year} is replaced each time with the drafts year, this is the url template.
url_template = "http://www.basketball-reference.com/draft/NBA_{year}.html"

#create the data frame
draft_df = pd.DataFrame()

#create a loop from 1996 to 2015 to scrape draft data, and add it to the draft.df frame using same techniques as above
for year in range(1966, 1967):
    #format will replace {year} in url_template with the current value of the year variable
    print(year)
    url = url_template.format(year=year)
    html = urlopen(url)
    soup = BeautifulSoup(html, 'html5lib')

    data_rows = soup.findAll('tr')[2:]
    player_data = [[td.getText() for td in data_rows[i].findAll('td')]
                       for i in range(len(data_rows))]

    #Turn the current year's data into a data frame
    year_df = pd.DataFrame(player_data, columns=column_headers)
    #Insert the "Draft_Yr" column as done previously
    year_df.insert(0, 'Draft_Yr', year)
    #append the created draft data frame for the current year into the overall draft data frame
    draft_df = draft_df.append(year_df, ignore_index=True)

                               
#Now we need to clean all of this new data, as done before
#NEED SOME WAY TO ITERATE OVER DRAFT COLUMNS TO CONVERT EACH ONE TO NUMERIC

#draft_df = draft_df.apply(pd.to_numeric, errors='coerce')
print("CONVERSION TO NUMERIC")
print(draft_df.values)
draft_df = draft_df[draft_df.Player.notnull()]
print("REMOVAL OF NULLS")
print(draft_df.values)
draft_df = draft_df.fillna(0)
print("REPLACE NaNs WITH ZEROS")
print(draft_df.values)
draft_df.rename(columns={'WS/48':'WS_per_48'}, inplace=True)
draft_df.columns = draft_df.columns.str.replace('%', '_Perc')
draft_df.columns.values[15:19] = [draft_df.columns.values[15:19][col] +  "_per_G" for col in range(4)]
#draft_df.loc[:,'Yrs':'AST'] = draft_df.loc[:,'Yrs':'AST'].astype(float64)
draft_df.drop('Rk', axis='columns', inplace=True)


#Finally, we should write all this data to a csv file to keep.
#to_csv(filename.csv) does this
draft_df.to_csv("draft_data_1996_to_2014.csv")
